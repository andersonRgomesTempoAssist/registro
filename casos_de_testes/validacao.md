##Casos de testes

> ####Verificar se o campo é obrigatório
1.  ✅ Realizar a busca do campo na estrutura
2.  ✅ Verificar se o campo possui dados
3.  ✅ Caso não tenha dado, colocar uma coluna de mensagem no dataframe com o código ``` '[001] Os seguintes campos não possuem valor <CAMPOS>'```
4.  ✅ Caso tenha dados, não fazer nada

> ####Verificar se o plano está cadastrado ou cancelado
1.  ✅ Realizar a busca dos planos cadastrados para o determinado cliente e armazenar no dataframe
```SELECT ID_PLANO, ID_STATUS FROM sgr.plano WHERE ID_CONTRATO = 36 ORDER BY ID_PLANO;```.
    
2.  Realizar uma varredura no dataframe para verificar se o plano informado existe na tabela
    1. ✅ Se não existir deve criticar a linha usando a mensagem ``` [002] Plano não cadastrado para estrutura ```
    2. ✅ Se existir porém o plano está cancelado deve criticar a linha usando a mensagem ``` [104] Plano cancelado no cadastro```
    
>####Comparar a data de inicio vigencia com a data final de vigencia
1.  ✅ Verificar se a data de inicio vigencia é maior que a data final vigenci
2.  ✅ Alterar o campo MENSAGEM_ERROR com o conteudo ```[005] Data fim enviada é menor que a Data inicio enviada```

>####Comprar a data final de vigencia com a data do inicio do contrato 
1. ✅ Realizar uma consulta com a tabela de contrato passando o id_contrato na query
2.  Realizar o fildro no dataframe verificando se a data de inicio do contrato é menor que a data fim vigencia
   ✅ 1. Se verdadeiro deverá inserir a mensagem no campo ```[014] Data Fim de Vigencia informada é menor que a Data de Inicio na USS```
    
>###Comparar se a data de inicio vigencia é maior que a data do sistema
1. ✅ Realizar uma busca se a data de inicio vigencia é maior que a data do sistema
caso encontre, deverá colocar a mensagem ```[016] Data de inicio de vigência enviada é maior que data de processamento```
   
   
>###Validar regra de status
✅  Verificar se a estrutura está habilitade para inclusão automática:
1. ✅  Caso a inclusão automática estiver verdadeiro:
   ✅  1.  realizar a query no dataframe e transformar o resultado APOLICE, APOLICEITEM e NUMEROCARTAO em ARRAY
   ✅  2.  com o resultado anterior realizar a seguinte query, tranformar em dataframe:
    ````    
        SELECT
            I.APOLICE,
            I.APOLICEITEM,
            I.NUMEROCARTAO,
            I.ID_STATUSITEMCOBERTO,
            I.DATAINICIOVIGENCIA,
            I.DATAFIMVIGENCIA,
            I.DATACADASTRO,
            I.DATAPROCESSAMENTO,
            I.DATACANCELAMENTO,
            P.CNPJCPFPROPR 
        FROM ITEMCOBERTO I
        INNER JOIN PROPRITEMCOBERTO P ON (I.ID_ITEMCOBERTO = P.ID_PROPRITEMCOBERTO)
        INNER JOIN PLANO PL ON (I.ID_PLANO = PL.ID_PLANO)
        INNER JOIN contrato c ON (PL.ID_CONTRATO =C.id_contrato)
        WHERE 0=0
          AND i.Id_Clientecorporativo=18
          AND pl.id_contrato= 36
          AND i.APOLICE IN ('008014670', '008014662', '003362176', '003484127' )
          AND i.APOLICEITEM  IN ('00001', '00001', '00001', '00001')
          AND i.NUMEROCARTAO IN ('1320930404FUN', '1320930428FUN', '1320930428FUN') 
    ````
    ✅ 3. Verificar se APOLICE, APOLICEITEM e NUMEROCARTAO existe no dataframe_juvo, usando com busca os dados do dataframe de entrada
        ✅ 1.  Não existe, deverá criar uma coluna de nome, TAREFA_A_EXECUTAR,  no dataframe a ser processado com com o valor de I de Inclusão
        ✅ 2.  Existi, flegar como A de alteração
   
>###Construir o update 
Nessa etapa, antes de realizar a construção realizar o envio do dataframe do item_coberto
1. Verificar se o id plano da carga é diferente da base
    1.  ✅  Se for igual:
        1. ✅ não faz nada
    2. se for diferente:
        1. Verficar se o Inicio de vigencia é menor ou igual que a data de processamento(Data corrente do sistema)    
            1.  caso positivo
                1.  ✅ A data de inicio de vigencia será a data de inicio informada no registro da carga
                2.  ✅ A data de inicio de cobrança será a data do processamento
                3.  A data de inicio e fim de cobrança será a data que vem no registro
                4.  A data de fim de vigencia e fim de cobrança serão as informadas no registro
                5.  ✅ Adicionar na entidade o registro antigo com o id de cancelamento e incluir um novo registro informando os dados acima
                   
            2. Caso contrário :
            <p> Nada será feito e adicionar a mensagem de erro ```[] Data de inicio de vigencia maior que a data do processamento```
                
        2. Verificar se a data de inicio de vigencia nova for maior com que está na base:
            1. Caso positivo a data de inicio de cobrança não deve ser alterada e a data de inicio vigencia alterada com a que tem no arquivo
            2. Caso contrário erro e gerar log
    
        3. Verificar se a data de final de vigencia 
            1. Se for menor que a data inicio vigencia da base :
               1. Caso positivo gerar uma mensagem de erro na linha ``` Mensagem ```
            2. Se a data final de vigencia informada é diferente da base:
                1.  caso positivo:
                    Atualizar os campos da base data fim de vigencia e data fim de cobrança
                2. caso negativo:
                    
            
            
        


