import configparser
import functools
import json
import datetime
import logging
import os

from multiprocessing import Pool, cpu_count

import pandas as pd
import pika
import pyreadstat
from kink import di
from collections import Counter
from datetime import date

from mult_process_fuctions import thread_property_covered_items, thread_covered_vehicles, thread_covered_life, \
    thread_property_covered, thread_particularities
from tools import send_queue_protocol, DateFormatEnum, get_resource_ini
from container import Container
from domain.usecases import ReadFileUseCase, LoadInformationUseCase, BuildInsertCoveredItemUseCase, \
    ExecuteInsertUpdate, \
    BuildUpdateUseCase, BuildPropertyCoveredItemUseCase, BuildCoveredVehicleUseCase, BuildCoveredLifeUseCase, \
    PropertyCoveredUseCase, ParticularityUseCase, SaveLogInputUseCase
from domain.usecases.validation.impl import NecessaryField, PlanValidation, DateValidate

LOGGER = logging.getLogger(__name__)


def callback_consumers(conn, ch, delivery_tag, body):
    LOGGER.info('Registration processing started at %s ', date.today())
    str_folder = body['file']
    LOGGER.debug("capture the main path " + str_folder)
    path_register_sav = os.path.join(str_folder, 'register.sav')
    LOGGER.debug("capture the path *.sav file " + path_register_sav)
    os.chdir(str_folder)
    os.chdir('../')
    path_structure_sav = os.path.join(os.getcwd(), 'structure.sav')
    contract_id = body['contract_id']
    structure_id = body['structure_id']
    client_id = body['client']
    id_log = body['id_log']
    LOGGER.debug("capture id log database, contract,  " + path_register_sav)
    log_input = di[SaveLogInputUseCase]
    input_log_model = log_input.find_by_id(id_log)
    LOGGER.debug("Log file founded, id_log " + str(id_log))
    di["id_log"] = id_log
    di["path"] = os.getcwd()
    di["delivery_tag"] = delivery_tag
    di["ch"] = ch
    step_load_information(path_register_sav, path_structure_sav, contract_id, structure_id, client_id, input_log_model)


def step_load_information(path_register_sav, path_structure_sav, contract_id, structure, client_id, input_log_model):
    LOGGER.info("start step load information")
    load_info = di[LoadInformationUseCase]
    plans_by_contract = load_info.load_information_plan_by_contract(contract_id)
    contract_by_id = load_info.load_contract_by_id(contract_id)
    structure_by_id = load_info.load_structure_by_id(structure)
    type_particularity = load_info.load_type_particularity()
    step_find_files(path_register_sav=path_register_sav, path_structure_sav=path_structure_sav,
                    contract_by_id=contract_by_id, plans_by_contract=plans_by_contract,
                    structure_by_id=structure_by_id, client_id=client_id, type_particularity=type_particularity,
                    input_log_model=input_log_model)


def step_find_files(path_register_sav, path_structure_sav, plans_by_contract, contract_by_id, structure_by_id,
                    client_id, type_particularity, input_log_model):
    LOGGER.info("start step finder and reader files")
    read_file_use_case = ReadFileUseCase(path_register_sav)
    df_registers = read_file_use_case.reader_file()
    read_file_use_case.set_path(path_structure_sav)
    df_structure = read_file_use_case.reader_file()
    step_validation_register(contract_by_id=contract_by_id, structure_data_frame=df_structure,
                             register_data_frame=df_registers,
                             plans_by_contract=plans_by_contract,
                             structure=structure_by_id, client_id=client_id,
                             type_particularity=type_particularity, input_log_model=input_log_model)


def step_validation_register(contract_by_id, structure_data_frame, register_data_frame, plans_by_contract, structure,
                             client_id, type_particularity, input_log_model):
    LOGGER.info("start step validation register")
    format_str = DateFormatEnum.__members__[structure.formatodata.replace("/", "")].value
    register_data_frame = NecessaryField(data_frame_register=register_data_frame,
                                         data_frame_structure=structure_data_frame).build_validation()
    register_data_frame = PlanValidation(data_frame=register_data_frame, plans=plans_by_contract).build_validation()
    register_data_frame = DateValidate(data_frame_register=register_data_frame,
                                       contract=contract_by_id, format_str=format_str).build_validation()
    step_validation_structure(structure=structure, contract_by_id=contract_by_id,
                              register_data_frame=register_data_frame, client_id=client_id,
                              structure_data_frame=structure_data_frame, type_particularity=type_particularity,
                              date_format=format_str, input_log_model=input_log_model)


def step_validation_structure(structure, contract_by_id, client_id, register_data_frame, structure_data_frame,
                              type_particularity, date_format, input_log_model):
    LOGGER.info("start step validation and check structure")
    date_contract = contract_by_id.datavalidade
    if structure.inclusao_automatica == 'S':
        load_info = di[LoadInformationUseCase]
        array_policy = register_data_frame['APOLICE'][register_data_frame['MESSAGE_ERROR'].isnull()].values
        array_policy_item = register_data_frame['APOLICEITEM'][register_data_frame['MESSAGE_ERROR'].isnull()].values
        array_car_number = register_data_frame['NUMEROCARTAO'][register_data_frame['MESSAGE_ERROR'].isnull()].values
        array_id_plano = register_data_frame['PLANO'][register_data_frame['MESSAGE_ERROR'].isnull()].values

        array_policy = [k for k, v in Counter(array_policy).items()]
        array_policy_item = [k for k, v in Counter(array_policy_item).items()]
        array_car_number = [k for k, v in Counter(array_car_number).items()]
        array_id_plano = [k for k, v in Counter(array_id_plano).items()]

        covered_item = load_info.load_covered_item(client_id, contract_by_id.id_contrato, array_policy,
                                                   array_policy_item,
                                                   array_car_number, array_id_plano)

        data_frame_covered_item = pd.DataFrame(data=covered_item)
        if 'JOB_TO_EXECUTE' not in register_data_frame.columns:
            register_data_frame['JOB_TO_EXECUTE'] = None

        if data_frame_covered_item.size > 0:
            data_frame_none_error = register_data_frame[register_data_frame['MESSAGE_ERROR'].isnull()]
            index = data_frame_none_error.query('APOLICE in ("' + '","'.join(
                data_frame_covered_item['apolice'].unique()) + '") and APOLICEITEM in ("' + '","'.join(
                data_frame_covered_item['apoliceitem'].unique()) + '") and NUMEROCARTAO in ("' + '","'.join(
                data_frame_covered_item['numerocartao'].unique()) + '")').index.values
            register_data_frame.at[index, 'JOB_TO_EXECUTE'] = 'A'

        index = register_data_frame.query("MESSAGE_ERROR.isnull() and JOB_TO_EXECUTE.isnull()").index.values
        register_data_frame.at[index, 'JOB_TO_EXECUTE'] = 'I'

        step_build_insert_update(register_data_frame, client_id, structure.id_estrutura, date_contract,
                                 data_frame_covered_item, structure_data_frame=structure_data_frame,
                                 type_particularity=type_particularity, date_format=date_format,
                                 input_log_model=input_log_model)


def step_build_insert_update(register_data_frame, id_client, id_structure, date_contract, data_frame_covered_item,
                             structure_data_frame, type_particularity, date_format, input_log_model):
    update_built = []
    if len(data_frame_covered_item) > 0:
        build_update = BuildUpdateUseCase(data_frame_covered_item=data_frame_covered_item, id_client=id_client,
                                          data_frame_register=register_data_frame, id_structure=id_structure,
                                          date_contract=date_contract, date_format=date_format,
                                          structure_data_frame=structure_data_frame)
        update_built = build_update.build_update()
    build_insert = BuildInsertCoveredItemUseCase(data_frame_register=register_data_frame, id_client=id_client,
                                                 id_structure=id_structure,
                                                 date_contract=date_contract, date_format=date_format,
                                                 structure_data_frame=structure_data_frame)
    insert_built = build_insert.build()

    step_execute_register(register_data_frame, insert_built, update_built, structure_data_frame, id_client,
                          type_particularity, date_format, input_log_model=input_log_model)


def step_execute_register(register_data_frame, insert_built, update_built, structure_data_frame, id_client,
                          type_particularity, date_format, input_log_model):
    execute_insert_update = di[ExecuteInsertUpdate]
    LOGGER.info("you have %s records to update ", len(update_built))
    if len(update_built) > 0:
        record_update = execute_insert_update.execute_update(update_built)
        build_complement_information(register_data_frame, structure_data_frame, record_update, id_client, 'UPDATE',
                                     type_particularity, date_format, input_log_model)
    LOGGER.info("you have %s records to include ", len(insert_built))
    if len(insert_built) > 0:
        record_insert = execute_insert_update.execute_insert(insert_built)
        build_complement_information(register_data_frame, structure_data_frame, record_insert, id_client, 'INSERT',
                                     type_particularity, date_format, input_log_model)

    path = di["path"]
    pyreadstat.write_sav(register_data_frame, path + "/logger/register.sav")

    payload = {
        "file": path + "/logger/register.sav",
        "id_log": di["id_log"]
    }
    send_queue_protocol(payload)
    ack_message(di["ch"], di["delivery_tag"])
    LOGGER.info('Registration processing finished at %s ', datetime.datetime.now())


def build_complement_information(register_data_frame, structure_data_frame, record_insert_update, id_client, is_update,
                                 type_particularity, date_format, input_log_model):
    build_property_covered_item = BuildPropertyCoveredItemUseCase()
    builder_particularity = ParticularityUseCase()
    builder_covered_vehicle = BuildCoveredVehicleUseCase()
    builder_property_covered = PropertyCoveredUseCase()
    build_covered_life_covered = BuildCoveredLifeUseCase(date_format=date_format)

    data_frame_covered_items = pd.DataFrame([s.__dict__ for s in record_insert_update])
    job = 'A' if is_update == 'UPDATE' else 'I'
    data_frame_register_none_error = register_data_frame.query(
        "MESSAGE_ERROR.isnull() and  JOB_TO_EXECUTE == '" + job + "'")

    indexs_verify_max_date = data_frame_covered_items[
        data_frame_covered_items.datafimvigencia > pd.Timestamp.max].index.values
    if len(indexs_verify_max_date) > 0:
        for index_ in indexs_verify_max_date:
            data_frame_covered_items.at[index_, 'datafimvigencia'] = pd.Timestamp.max.date()
    data_frame_merge = pd.merge(left=data_frame_covered_items, right=data_frame_register_none_error,
                                left_on=['apolice', 'apoliceitem', 'numerocartao'],
                                right_on=['APOLICE', 'APOLICEITEM', 'NUMEROCARTAO'],
                                how='inner')

    pool = Pool(processes=max(cpu_count() - 1, 1))
    if 'PROPRITEMCOBERTO' in structure_data_frame['tabelaDestino'].values:
        pool.apply(func=thread_property_covered_items,
                   args=(build_property_covered_item, data_frame_merge, structure_data_frame, id_client))

    if 'VEICULOCOBERTO' in structure_data_frame['tabelaDestino'].values:
        pool.apply(func=thread_covered_vehicles,
                   args=(builder_covered_vehicle, data_frame_merge, structure_data_frame, id_client))

    if 'VIDACOBERTA' in structure_data_frame['tabelaDestino'].values:
        pool.apply(func=thread_covered_life,
                   args=(build_covered_life_covered, data_frame_merge,
                         structure_data_frame,
                         id_client))

    if 'IMOVELCOBERTO' in structure_data_frame['tabelaDestino'].values:
        pool.apply(func=thread_property_covered,
                   args=(builder_property_covered, data_frame_merge,
                         structure_data_frame,
                         id_client))

    if 'PARTICULARIDADE' in structure_data_frame['tabelaDestino'].values:
        pool.apply(func=thread_particularities,
                   args=(builder_particularity, data_frame_merge, structure_data_frame,
                         type_particularity))

    pool.close()
    pool.join()


def ack_message(ch, delivery_tag):
    """Note that `ch` must be the same pika channel instance via which
    the message being ACKed was retrieved (AMQP protocol constraint).
    """
    if ch.is_open:
        ch.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        pass


def do_work(conn, ch, delivery_tag, body):
    obj = json.loads(body.decode('UTF-8'))
    LOGGER.info(" [x] Start Process - Register %r" % obj)

    callback_consumers(conn, ch, delivery_tag, obj)


def on_message(ch, method_frame, _header_frame, body, args):
    (conn, callback_consumers) = args
    delivery_tag = method_frame.delivery_tag
    do_work(conn, ch, delivery_tag, body)


if __name__ == '__main__':
    Container()
    config = configparser.ConfigParser()
    config.read(get_resource_ini())
    host = config["queue"]["HOST"]
    user = config["queue"]["USER"]
    password = config["queue"]["PASSWORD"]
    port_ = int(config["queue"]["PORT"])
    credentials = pika.PlainCredentials(user, password)

    parameters = pika.ConnectionParameters(
        host=host, port=port_, credentials=credentials, heartbeat=0)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    threads = []
    on_message_callback = functools.partial(on_message, args=(connection, do_work))
    channel.basic_consume('process.register', on_message_callback)

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    # Wait for all to complete
    for thread in threads:
        thread.join()

    connection.close()
