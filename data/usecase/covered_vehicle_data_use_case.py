
class CoveredVehicleDataUseCase:
    def __init__(self, repository):
        self.repository = repository

    def insert(self, entities):
        return self.repository.insert(entities)

    def update(self, entities):
        return self.repository.update(entities)