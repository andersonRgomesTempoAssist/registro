class CoveredLivesDataUseCase:
    def __init__(self, repository):
        self.repository = repository

    def insert(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.insert(entities)

    def update(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.update(entities)