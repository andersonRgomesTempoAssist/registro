from data.helpers import ExceptionData


class StructureDataUseCase:

    def __init__(self, repository):
        self.repository = repository

    def find_by_id(self, id_structure):
        if id_structure is None:
            raise ExceptionData(0, 'id_client is necessary')

        return self.repository.find_by_id(id_structure)
