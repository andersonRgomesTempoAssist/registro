from collections import namedtuple

InputLogModel = namedtuple(
    "InputLogModel", [
        'id_seqlog',
        'id_clientecorporativo',
        'id_contrato',
        'data',
        'id_tipocarteira',
        'hora',
        'arquivoorigem',
        'totalregistros',
        'regcorretos',
        'regincorretos',
        'regenvinclusao',
        'regenvalteracao',
        'regenvcancelamento',
        'regenvreativacao',
        'regenvprecadastro',
        'regenverro',
        'regenvfatal1',
        'regenvfatal2',
        'regenvvencido',
        'regenvlote',
        'data_fim',
        'hora_fim',
        'status',
        'id_estrutura',
        'regjacadastrado',
        'regnaoprocessado',
        'regignorado',
        'regduplicadocarga']
)
