import logging
import logging.config
from container import Container

LOGGER = logging.getLogger(__name__)


def thread_property_covered_items(build_property_covered_item, register_data_frame,
                                  structure_data_frame,
                                  id_client):
    LOGGER.info("Start Process property_covered_items")
    property_covered_items = build_property_covered_item.build(register_data_frame,
                                                               structure_data_frame,
                                                               id_client)
    if property_covered_items is not None and len(property_covered_items) > 0:
        Container()
        return build_property_covered_item.execute_update(property_covered_items)
    LOGGER.info("Finished Process property_covered_items")


def thread_covered_vehicles(builder_covered_vehicle, register_data_frame,
                            structure_data_frame,
                            id_client):
    LOGGER.info("Start Process covered_vehicles")
    covered_vehicles = builder_covered_vehicle.build(register_data_frame,
                                                     structure_data_frame,
                                                     id_client)
    if covered_vehicles is not None and len(covered_vehicles) > 0:
        Container()
        return builder_covered_vehicle.execute_update(covered_vehicles)

    LOGGER.info("Finished Process covered_vehicles")


def thread_covered_life(build_covered_life_covered, register_data_frame,
                        structure_data_frame,
                        id_client):
    LOGGER.info("Start Process covered_lives")
    covered_lives = build_covered_life_covered.build(register_data_frame,
                                                     structure_data_frame,
                                                     id_client)
    if covered_lives is not None and len(covered_lives) > 0:
        Container()
        return build_covered_life_covered.execute_update(covered_lives)

    LOGGER.info("Finished Process covered_lives")


def thread_property_covered(builder_property_covered, register_data_frame,
                            structure_data_frame,
                            id_client):
    LOGGER.info("Start Process thread_property_covered")
    property_covered = builder_property_covered.build(register_data_frame,
                                                      structure_data_frame,
                                                      id_client)
    if property_covered is not None and len(property_covered) > 0:
        Container()
        return builder_property_covered.execute_update(property_covered)

    LOGGER.info("Finished Process thread_property_covered")


def thread_particularities(builder_particularity, register_data_frame, structure_data_frame,
                           type_particularity):
    Container()
    LOGGER.info("Start Process thread_particularities")
    list_item_coberto = list(
        register_data_frame.query('id_itemcoberto.isnull() == False')['id_itemcoberto'].astype(int))
    particularities = builder_particularity.build(register_data_frame, structure_data_frame,
                                                  type_particularity, list_item_coberto)
    if particularities is not None and len(particularities) > 0:
        return builder_particularity.execute_update(particularities)

    LOGGER.info("Finished Process thread_particularities")
