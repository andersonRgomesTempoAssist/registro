import configparser
import os

from kink import di
from tools import get_resource_ini
from data.usecase import PlanoDataUseCase, ContractDataUseCase, StructureDataUseCase, TipoParticularidadeUseCase, \
    ParticularityDataUseCase, PropertyCoveredDataUseCase, CoveredLivesDataUseCase
from data.usecase.covered_vehicle_data_use_case import CoveredVehicleDataUseCase
from data.usecase.item_coberto_data_usecase import CoveredItemDataUseCase
from data.usecase.property_covered_item_data_use_case import PropertyCoveredItemUseCase
from domain.usecases import LoadInformationUseCase, ExecuteInsertUpdate, SaveLogInputUseCase
from data.usecase.log_entrada_usecase import LogEntradaUseCase
from infra.config import DBConnectionHandler
from infra.repositories import PlanoRepository, ContractRepository, PropertyCoveredItemRepository, \
    CoveredItemRepository, StructureRepository, ParticularityRepository, PropertyCoveredRepository, LogEntradaRepository
from infra.repositories.covered_lives_repository import CoveredLivesRepository
from infra.repositories.covered_vehicle_repository import CoveredVehicleRepository
from infra.repositories.particularity_type_repository import ParticularityTypeRepository


class Container:
    """Class Container to dependency injection"""

    def __init__(self) -> None:
        config = configparser.ConfigParser()
        config.read(get_resource_ini())
        di["lib_dir"] = os.environ[(config["database"]["LIB_DIR"])]
        di["string_connection"] = config["database"]["STRING_CONNECTION"]

        di['plano_repository'] = lambda di: PlanoRepository(db_connection=DBConnectionHandler.__session__())
        di['contract_repository'] = lambda di: ContractRepository(db_connection=DBConnectionHandler.__session__())
        di['property_covered_item_repository'] = lambda di: PropertyCoveredItemRepository(
            db_connection=DBConnectionHandler.__session__())
        di['structure_repository'] = lambda di: StructureRepository(db_connection=DBConnectionHandler.__session__())
        di['covered_item_repository'] = lambda di: CoveredItemRepository(
            db_connection=DBConnectionHandler.__session__())
        di['particularity_type_repository'] = lambda di: ParticularityTypeRepository(
            db_connection=DBConnectionHandler.__session__())
        di['particularity_repository'] = lambda di: ParticularityRepository(
            db_connection=DBConnectionHandler.__session__())
        di['covered_vehicle_repository'] = lambda di: CoveredVehicleRepository(
            db_connection=DBConnectionHandler.__session__())
        di['property_covered_repository'] = lambda di: PropertyCoveredRepository(
            db_connection=DBConnectionHandler.__session__())
        di['covered_lives_repository'] = lambda di: CoveredLivesRepository(
            db_connection=DBConnectionHandler.__session__())

        di[PlanoDataUseCase] = lambda di: PlanoDataUseCase(repository=di['plano_repository'])
        di[ContractDataUseCase] = lambda di: ContractDataUseCase(repository=di['contract_repository'])
        di[CoveredItemDataUseCase] = lambda di: CoveredItemDataUseCase(repository=di['covered_item_repository'])
        di[StructureDataUseCase] = lambda di: StructureDataUseCase(repository=di['structure_repository'])
        di[TipoParticularidadeUseCase] = lambda di: TipoParticularidadeUseCase(
            repository=di['particularity_type_repository'])
        di[PropertyCoveredItemUseCase] = lambda di: PropertyCoveredItemUseCase(
            repository=di['property_covered_item_repository'])
        di[ParticularityDataUseCase] = lambda di: ParticularityDataUseCase(repository=di['particularity_repository'])
        di[CoveredVehicleDataUseCase] = lambda di: CoveredVehicleDataUseCase(
            repository=di['covered_vehicle_repository'])
        di[PropertyCoveredDataUseCase] = lambda di: PropertyCoveredDataUseCase(
            repository=di['property_covered_repository'])
        di[CoveredLivesDataUseCase] = lambda di: CoveredLivesDataUseCase(
            repository=di['covered_lives_repository'])
        di[LogEntradaRepository] = lambda di: LogEntradaRepository()
        di[LogEntradaUseCase] = lambda di: LogEntradaUseCase()
        di[LoadInformationUseCase] = lambda di: LoadInformationUseCase()
        di[ExecuteInsertUpdate] = lambda di: ExecuteInsertUpdate()
        di[SaveLogInputUseCase] = lambda di: SaveLogInputUseCase()
