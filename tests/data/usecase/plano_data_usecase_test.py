import unittest
from unittest.mock import MagicMock

from faker import Faker

from data.helpers import ExceptionData
from data.usecase import PlanoDataUseCase
from infra.entities import PlanoEntity
from infra.helpers import ExceptionRepository


class PlanoDataUseCaseTest(unittest.TestCase):
    def setUp(self) -> None:
        self.faker = Faker()
        self.repository = MagicMock()
        self.sut = PlanoDataUseCase(repository=self.repository)

    def test_find_by_id(self):
        """ Should find by id success"""
        id_plan = self.faker.random_int(4)
        self.repository.find_by_id = MagicMock(
            return_value=PlanoEntity(id=id_plan, id_contrato=self.faker.random_int(4),
                                     id_status=self.faker.random_int(2),
                                     titulo=self.faker.text(40),
                                     centrocusto=self.faker.text(10),
                                     iniciovigencia=self.faker.date_time(),
                                     fimvigencia=self.faker.date_time()))

        result = self.sut.find_by_id(id_plan)
        self.assertEqual(result.id_plano, id_plan)

    def test_find_by_id_exception_idNotFound(self):
        """ Should find by id but return not found plan"""
        self.repository.find_by_id = MagicMock(return_value=ExceptionRepository('000', 'TEST NOT FOUND'))
        self.assertRaises(ExceptionData, self.sut.find_by_id, None)

    def test_find_by_id_exception_internal_error(self):
        """ Should find by id but return not found plan"""
        self.repository.find_by_id = MagicMock(return_value=Exception('000', 'Internal Server Error'))
        self.assertRaises(ExceptionData, self.sut.find_by_id, None)

    def test_find_by_id_exception_idPlanNone(self):
        """Should exception if id plan is none"""
        self.assertRaises(ExceptionData, self.sut.find_by_id, None)

    def test_find_by_id_contract(self):
        """ Should find by id success"""
        id_contract = self.faker.random_int(4)
        self.repository.find_by_id_contract = MagicMock(
            return_value=[PlanoEntity(id=self.faker.random_int(4), id_contrato=id_contract,
                                      id_status=self.faker.random_int(2),
                                      titulo=self.faker.text(40),
                                      centrocusto=self.faker.text(10),
                                      iniciovigencia=self.faker.date_time(),
                                      fimvigencia=self.faker.date_time()),
                          PlanoEntity(id=self.faker.random_int(4), id_contrato=id_contract,
                                      id_status=self.faker.random_int(2),
                                      titulo=self.faker.text(40),
                                      centrocusto=self.faker.text(10),
                                      iniciovigencia=self.faker.date_time(),
                                      fimvigencia=self.faker.date_time())
                          ])

        result = self.sut.find_by_id_contract(id_contract)
        self.assertEqual(result[0].id_contrato, id_contract)

    def test_find_by_id_contract_exception_NotFound(self):
        """ Should find by id but return not found plan"""
        self.repository.find_by_id_contract = MagicMock(return_value=ExceptionRepository('000', 'TEST NOT FOUND'))
        self.assertRaises(ExceptionData, self.sut.find_by_id, None)

    def test_find_by_id_exception_internal_error(self):
        """ Should find by id but return not found plan"""
        self.repository.find_by_id_contract = MagicMock(return_value=Exception('000', 'Internal Server Error'))
        self.assertRaises(ExceptionData, self.sut.find_by_id, None)

    def test_find_by_id_exception_idContractNone(self):
        """Should exception if id contract is none"""
        self.assertRaises(ExceptionData, self.sut.find_by_id_contract, None)


if __name__ == '__main__':
    unittest.main()
