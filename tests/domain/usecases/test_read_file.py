import unittest

from domain.hepers import ExceptionDomain
from domain.usecases import ReadFileUseCase


class ReadFileTest(unittest.TestCase):
    """Class test to read file SAV"""

    def test_read_file_success(self):
        """Should to read file SAV and convert Pandas DataFrame"""

        read_file_test = ReadFileUseCase("/Users/anderson_gomes/juvo/process_output/register/18/36/5441/register.sav")
        data_frame = read_file_test.reader_file()
        self.assertTrue(len(data_frame) > 0)

    def test_read_file_error_not_exists(self):
        """Should to read file SAV, but exception not Found"""

        read_file_test = ReadFileUseCase("/Users/anderson_gomes/juvo/process_output/register/18/36/5441/not_found.sav")
        self.assertRaises(ExceptionDomain, read_file_test.reader_file)

    def test_read_file_error_type_incorrect(self):
        read_file = ReadFileUseCase("/Users/anderson_gomes/juvo/process_output/register/18/36/5441/test_records.txt")
        self.assertRaises(ExceptionDomain, read_file.reader_file)


if __name__ == '__main__':
    unittest.main()
