class DataParams:

    def __init__(self, file, client, structure_id, contract_id):
        self._file = file
        self._client = client
        self._structure_id = structure_id
        self._contract_id = contract_id

    def get_file(self):
        return self._file

    def get_client(self):
        return self._client

    def get_structure_id(self):
        return self._structure_id

    def get_contract_id(self):
        return self._contract_id
