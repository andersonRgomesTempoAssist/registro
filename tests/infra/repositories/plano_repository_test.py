import unittest
from sqlalchemy import exc
from faker import Faker
from mock_alchemy.mocking import AlchemyMagicMock, UnifiedAlchemyMagicMock

from infra.entities import PlanoEntity
from infra.helpers import ExceptionRepository
from infra.repositories import PlanoRepository


class PlanoRepositoryTest(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_find_by_id(self):
        fake = Faker()
        id_plan = fake.random_int(4)
        session = UnifiedAlchemyMagicMock()
        session.add(PlanoEntity(id=id_plan, id_contrato=fake.random_int(4), id_status=fake.random_int(2),
                                titulo=fake.text(40), centrocusto=fake.text(10), iniciovigencia=fake.date_time(),
                                fimvigencia=fake.date_time()))
        repository = PlanoRepository([session])
        entity = repository.find_by_id(id_plan)
        self.assertEqual(entity.id, id_plan)

    def test_find_by_id_not_found(self):
        fake = Faker()
        id_plan = fake.random_int(4)
        session = UnifiedAlchemyMagicMock()
        repository = PlanoRepository([session])
        self.assertRaises(ExceptionRepository, repository.find_by_id, id_plan)

    def test_find_by_id_internal_error(self):
        """ Test find internal error """
        fake = Faker()
        id_plan = fake.random_int(4)
        session_mock = AlchemyMagicMock()
        session_mock.query.side_effect = exc.OperationalError(params="TEST", orig="TEST", statement="TEST")
        repository = PlanoRepository([session_mock])
        self.assertRaises(ExceptionRepository, repository.find_by_id, id_plan)

    def test_find_by_id_contract(self):
        fake = Faker()
        id_plan = fake.random_int(4)
        id_contract = fake.random_int(4)
        session = UnifiedAlchemyMagicMock()
        session.add(PlanoEntity(id=id_plan, id_contrato=id_contract, id_status=fake.random_int(2),
                                titulo=fake.text(40), centrocusto=fake.text(10), iniciovigencia=fake.date_time(),
                                fimvigencia=fake.date_time()))
        repository = PlanoRepository([session])
        entity = repository.find_by_id_contract(id_contract)
        self.assertEqual(entity[0].id_contrato, id_contract)

    def test_find_by_id_contract_not_found(self):
        fake = Faker()
        id_contract = fake.random_int(4)
        session = UnifiedAlchemyMagicMock()
        repository = PlanoRepository([session])
        self.assertRaises(ExceptionRepository, repository.find_by_id_contract, id_contract)

    def test_find_by_id_contract_internal_error(self):
        """ Test find internal error """
        fake = Faker()
        id_contract = fake.random_int(4)
        session_mock = AlchemyMagicMock()
        session_mock.query.side_effect = exc.OperationalError(params="TEST", orig="TEST", statement="TEST")
        repository = PlanoRepository([session_mock])
        self.assertRaises(ExceptionRepository, repository.find_by_id_contract, id_contract)


if __name__ == '__main__':
    unittest.main()
