import unittest
from kink import di

from src import Container


class CoveredItemRepositoryTest(unittest.TestCase):

    def test_find_data_success(self):
        """Should call find_covered_item success"""
        Container()
        sut = di['covered_item_repository']
        result = sut.find_covered_item(id_client=18, id_contract=36,
                                       array_policy=['008014670', '008014662', '003362176',
                                                     '003484127'],
                                       array_policy_item=['00001', '00001', '00001', '00001'],
                                       array_car_number=['1320930404FUN', '1320930428FUN',
                                                         '1320930428FUN'])
        self.assertTrue(len(result) > 0)


if __name__ == '__main__':
    unittest.main()
