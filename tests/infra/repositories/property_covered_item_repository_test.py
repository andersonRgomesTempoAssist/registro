import unittest
from kink import di

from src import Container


class PropertyCoveredItemRepositoryTest(unittest.TestCase):

    def test_find_by_id(self):
        """Should test find by id return success"""
        Container()
        property_covered_repo = di['property_covered_item_repository']
        test = property_covered_repo.find_by_id(184601874)
        print(test)


if __name__ == '__main__':
    unittest.main()
