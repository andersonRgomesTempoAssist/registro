
from sqlalchemy import Column, String, Integer, Date
from infra.config import Base


class ContractEntity(Base):
    __tablename__ = 'CONTRATO'

    id_contrato = Column("ID_CONTRATO", Integer, primary_key=True)
    id_status = Column("ID_STATUS", Integer)
    id_clientecorporativo = Column("ID_CLIENTECORPORATIVO", Integer)
    numero = Column("NUMERO", String)
    titulo = Column("TITULO", String)
    datainicio = Column("DATAINICIO", Date)
    datavalidade = Column("DATAVALIDADE", Date)
    ce_crocusto = Column("CENTROCUSTO", Integer)
    cnpjclicorp = Column("CNPJCLICORP", String)
    razaosocialuss = Column("RAZAOSOCIALUSS", String)
    razaosocialclicorp = Column("RAZAOSOCIALCLICORP", String)
    id_cmpresa = Column("ID_EMPRESA", Integer)
    datacadastro = Column("DATACADASTRO", Date)
    usuariocadastro = Column("USUARIOCADASTRO", String)
    cnpjuss = Column("CNPJUSS", String)
    itemcobertoapolicecliente = Column("ITEMCOBERTOAPOLICECLIENTE", String)
    carroconectado = Column("CARROCONECTADO", String)
