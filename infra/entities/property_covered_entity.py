from sqlalchemy import Column, String, Integer
from infra.config import Base


class PropertyCoveredEntity(Base):
    __tablename__ = "IMOVELCOBERTO"

    id_imovelcoberto = Column("ID_IMOVELCOBERTO", Integer, primary_key=True)
    pais = Column("PAIS", String)
    uf = Column("UF", String)
    cidade = Column("CIDADE", String)
    bairro = Column("BAIRRO", String)
    cep = Column("CEP", String)
    logradouro = Column("LOGRADOURO", String(60))
    numero = Column("NUMERO", String)
    complemento = Column("COMPLEMENTO", String(100))
    id_clientecorporativo = Column("ID_CLIENTECORPORATIVO", Integer)
    codigoimovel = Column("CODIGOIMOVEL", String)
    nomeinquilino = Column("NOMEINQUILINO", String)
    cnpjcpfinquilino = Column("CNPJCPFINQUILINO", String)
