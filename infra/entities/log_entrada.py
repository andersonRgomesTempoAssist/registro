from sqlalchemy import Column, String, Integer, Date, Sequence
from infra.config import Base


class LogEntradaEntity(Base):
    __tablename__ = "LOGENTRADA"

    id_seq = Sequence('ID_SEQLOG', metadata=Base.metadata)
    id_seqlog = Column("ID_SEQLOG", Integer(), id_seq,  primary_key=True)
    id_clientecorporativo = Column("ID_CLIENTECORPORATIVO", Integer())
    id_produto = Column("ID_PRODUTO", Integer())
    id_contrato = Column("ID_CONTRATO", Integer())
    data = Column("DATA", Date)
    id_tipocarteira = Column("ID_TIPOCARTEIRA", Integer())
    hora = Column("HORA", Date)
    arquivoorigem = Column("ARQUIVOORIGEM", String(4000))
    totalregistros = Column("TOTALREGISTROS", Integer())
    regcorretos = Column("REGCORRETOS", Integer())
    regincorretos = Column("REGINCORRETOS", Integer())
    regenvinclusao = Column("REGENVINCLUSAO", Integer())
    regenvalteracao = Column("REGENVALTERACAO", Integer())
    regenvcancelamento = Column("REGENVCANCELAMENTO", Integer())
    regenvreativacao = Column("REGENVREATIVACAO", Integer())
    regenvprecadastro = Column("REGENVPRECADASTRO", Integer())
    regenverro = Column("REGENVERRO", Integer())
    arquivodestino = Column("ARQUIVODESTINO", String(4000))
    regenvfatal1 = Column("REGENVFATAL1", Integer())
    regenvfatal2 = Column("REGENVFATAL2", Integer())
    regenvvencido = Column("REGENVVENCIDO", Integer())
    regenvlote = Column("REGENVLOTE", Integer())
    data_fim = Column("DATA_FIM", Date)
    hora_fim = Column("HORA_FIM", Date)
    status = Column("STATUS", String(10))
    id_estrutura = Column("ID_ESTRUTURA", Integer())
    regjacadastrado = Column("REGJACADASTRADO", Integer())
    regnaoprocessado = Column("REGNAOPROCESSADO", Integer())
    regignorado = Column("REGIGNORADO", Integer())
    regduplicadocarga = Column("REGDUPLICADOCARGA", Integer())
