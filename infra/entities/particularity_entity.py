from sqlalchemy import Column, String, BigInteger, Integer, Sequence
from infra.config import Base


class ParticularityEntity(Base):
    __tablename__ = "PARTICULARIDADE"

    id_seq = Sequence('ID_PARTICULARIDADE', metadata=Base.metadata)
    id_particularidade = Column("ID_PARTICULARIDADE", BigInteger, id_seq, nullable=False, primary_key=True)
    id_itemcoberto = Column("ID_ITEMCOBERTO", Integer, nullable=False)
    id_tipoparticularidade = Column("ID_TIPOPARTICULARIDADE", Integer, nullable=False)
    valor = Column("VALOR", String(60), nullable=False)