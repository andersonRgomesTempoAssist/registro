from infra.entities import StructureEntity
import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound
from infra.config import DBConnectionHandler
from collections import namedtuple

class StructureRepository:
    def __init__(self, db_connection):
        self.db_connection = db_connection

    def find_by_id(self, id_structure):
        for i in DBConnectionHandler.__session__():
            try:
                result = i.query(StructureEntity).filter(
                    StructureEntity.id_estrutura == id_structure).one()
                Structure = namedtuple(
                    "Structure",
                    [
                        "id_estrutura", "id_cliente", "id_contrato", "inclusao_automatica", "endosso",
                        "vigenciafechada",
                        "dia_faturamento", "formatodata"
                    ])
                return Structure(id_estrutura=result.id_estrutura,
                                 id_cliente=result.id_clientecorporativo,
                                 id_contrato=result.id_contrato,
                                 inclusao_automatica=result.inclusaoautomatica,
                                 endosso=result.endosso,
                                 vigenciafechada=result.vigenciafechada,
                                 dia_faturamento=result.dia_faturamento,
                                 formatodata=result.formatodata)

            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
