import logging

import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound
from collections import namedtuple

from infra.entities import ContractEntity
from infra.helpers import ExceptionRepository
from infra.config import DBConnectionHandler

LOGGER = logging.getLogger(__name__)


class ContractRepository:

    def __init__(self, db_connection):
        self.db_connection = db_connection

    def find_by_id(self, id_contract):
        LOGGER.debug("Find contract by id , id: %s", id_contract)
        with DBConnectionHandler() as db_connection:
            try:
                result = db_connection.session.query(ContractEntity).filter(
                    ContractEntity.id_contrato == id_contract).one()
                Contrato = namedtuple(
                    "Contrato",
                    [
                        "id_contrato",
                        "id_status",
                        "id_clientecorporativo",
                        "numero",
                        "titulo",
                        "datainicio",
                        "datavalidade",
                        "datacadastro"
                    ])
                LOGGER.debug("Contract founder: contract id %s and status is %s", result.id_contrato, result.id_status)
                return Contrato(id_contrato=result.id_contrato, id_status=result.id_status,
                                id_clientecorporativo=result.id_clientecorporativo,
                                numero=result.id_clientecorporativo,
                                titulo=result.titulo, datainicio=result.datainicio,
                                datavalidade=result.datavalidade, datacadastro=result.datacadastro)
            except sqlalchemy.exc.OperationalError as error:
                LOGGER.error("SQLAlchemy error , %s", error)
                raise ExceptionRepository('000', 'Internal server Error')
            except NoResultFound:
                LOGGER.error("Contract Not Found")
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                LOGGER.error("Exception error , %s", error)
                raise ExceptionRepository('000', 'Internal server Error')
            finally:
                db_connection.session.close()
